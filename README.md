# PubMed metadata downloader

Downloads metadata about all PubMed studies that match a search term into a `json` file. **Has no external dependencies.**

## Usage

    ./pmid_downloader.py [search term]

The name of the saved file can be customized by editing the script.

## Miscellaneous

* Respects PubMed API limits
* Saves to the file every 100 studies. In case of interruption, can resume
  right back up! Will only download the studies that are not present in the
  file.
