import requests
import json
import time

storage_filename = 'pmid_storage.json'

def load_data():
    """
    Loads stored data if it exists.
    """
    global stored_info

    try:
        with open(storage_filename) as f:
            stored_info = json.load(f)

        print('Loaded data')
    except:
        stored_info = {}


def store_data():
    """
    Writes to the json file
    """
    global stored_info

    with open(storage_filename, 'w') as f:
        json.dump(stored_info, f)

    print('Stored data')

def pmid_search(search_term):
    """
    Searches PubMed for the search term and returns a list of PMIDs that match.
    """

    base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?'
    database = 'pubmed'
    return_format = 'json'
    max_results = '10000'

    database_string = 'db=' + database
    return_format_string = 'retmode=' + return_format
    max_results_string = 'retmax=' + max_results
    term_string = 'term=' + '+'.join(search_term.split(' '))

    request_link = base + '&'.join([database_string,
        return_format_string, max_results_string, term_string])

    api_response = requests.get(request_link).json()

    return api_response['esearchresult']['idlist']

class pmid_info():
    """
    Searches PubMed for a PMID and returns metadata about it
    """

    def __init__(self, pmid):

        base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?'
        database = 'pubmed'
        return_format = 'json'

        database_string = 'db=' + database
        return_format_string = 'retmode=' + return_format
        pmid_string = 'id=' + str(pmid)

        request_link = base + '&'.join([database_string,
            return_format_string, pmid_string])

        api_response = requests.get(request_link).json()

        self.raw_data = api_response['result'][pmid]

load_data()


i = 0

for x in pmid_search(input("Search term: ")):
    if x in stored_info:
        print(x + " was already found!")
    else:
        if i % 100 == 0: store_data()
        data = pmid_info(x).raw_data
        print('Retrieved ' + x)
        stored_info[x] = data
        time.sleep(0.333)
        i = i+1

store_data()
